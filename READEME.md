# Bot Telegram Node.js

Ce projet est un bot Telegram développé en Node.js utilisant Express et node-fetch pour gérer les webhooks et interagir avec l'API de Telegram.

## Prérequis

- Node.js (version 12 ou supérieure)
- npm (gestionnaire de paquets Node.js)
- Un bot Telegram (créé avec [BotFather](https://telegram.me/botfather))

## Installation

1. Clonez ce dépôt sur votre machine locale :

    ```sh
    git clone https://github.com/votre-utilisateur/votre-repo.git
    cd votre-repo
    ```

2. Installez les dépendances nécessaires :

    ```sh
    npm install
    ```

3. Créez un fichier `.env` à la racine de votre projet et ajoutez votre token Telegram :

    ```env
   TELEGRAM_TOKEN=YOUR_TELEGRAM_BOT_TOKEN
   EXPOSE_URL=YOUR_EXPOSE_URL
   DISCORD_URL=YOUR_DISCORD_WEBHOOK_URL
   ```

## Utilisation en Développement

Pour exposer votre serveur local et configurer le webhook de Telegram, vous pouvez utiliser [LocalTunnel](https://localtunnel.github.io/www/).

1. Installez LocalTunnel globalement :

    ```sh
    npm install -g localtunnel
    ```

2. Démarrez votre serveur Node.js :

    ```sh
    npm start
    ```

3. Exposez votre serveur avec LocalTunnel :

    ```sh
    lt --port 5000 --subdomain mycustomsubdomain
    ```

   Remplacez `mycustomsubdomain` par un sous-domaine de votre choix. LocalTunnel vous fournira une URL publique comme `https://mycustomsubdomain.loca.lt`.

## Structure du Projet

```plaintext
.
├── index.mjs            # Point d'entrée principal du bot
├── package.json         # Fichier de configuration npm
├── package-lock.json    # Fichier de verrouillage des dépendances
└── .env                 # Fichier d'environnement pour le token Telegram
