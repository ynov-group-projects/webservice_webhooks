import 'dotenv/config'
import express from 'express';
import fetch from 'node-fetch';

const app = express();
const port = process.env.PORT || 5000;
const telegramToken = process.env.TELEGRAM_TOKEN;
const discordURL = process.env.DISCORD_URL;
const exposeURL = process.env.EXPOSE_URL;

app.use(express.json());

// Endpoint pour recevoir les messages de Telegram
app.post('/webhook', (req, res) => {
    const message = req.body;

    // Affiche les messages dans la console
    // console.log('Message reçu:', message);

    // Envoie du message sur Discord
    sendMessage(message);

    res.sendStatus(200);
});

function sendMessage(message) {
    fetch(discordURL, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({"username": "WebserviceWebhook", "content": `Nouveau message sur le bot Telegram de ${message.message.from.first_name}:\n${message.message.text}`})
    }).then(response => {
        if (response.ok) {
            console.log('Message envoyé avec succès');
        } else {
            console.error('Erreur lors de l\'envoi du message');
        }
    }).catch(error => {
        console.error('Erreur lors de l\'envoi du message:', error);
    })
}

// Configurer le webhook de Telegram
const setWebhookTelegram = async () => {
    const url = `https://api.telegram.org/bot${telegramToken}/setWebhook?url=${exposeURL}/webhook`;
    const response = await fetch(url);

    if (response.ok) {
        console.log('Webhook configuré avec succès');
    } else {
        console.error('Erreur lors de la configuration du webhook');
    }
};

// Configurer le webhook de Discord
const setWebhookDiscord = async () => {
    const response = await fetch(discordURL);

    if (response.ok) {
        console.log('Webhook Discord configuré avec succès');
    }
    else {
        console.error('Erreur lors de la configuration du webhook Discord');
    }
}

// Démarrer le serveur et configurer le webhook
app.listen(port, () => {
    console.log(`Serveur en écoute sur le port ${port}`);
    setWebhookTelegram();
    setWebhookDiscord();
});
